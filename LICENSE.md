/fermarsan/guia-para-usar-linux-sobre-android

tiene licencia bajo el tipo

### Licencia MIT

Una licencia permisiva breve y simple con condiciones que solo requieren la preservación de los avisos de derechos de autor y licencia. Los trabajos con licencia, las modificaciones y los trabajos más grandes pueden distribuirse bajo diferentes términos y sin código fuente.

### Permisos

- Uso comercial
- Modificación
- Distribución
- Uso privado

### Limitaciones

- Responsabilidad
- Garantía

### Condiciones

- Aviso de licencia y copyright
